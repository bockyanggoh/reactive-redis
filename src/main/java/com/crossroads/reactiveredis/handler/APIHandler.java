package com.crossroads.reactiveredis.handler;

import com.crossroads.reactiveredis.RoomService;
import com.crossroads.reactiveredis.datamodel.Room;
import com.crossroads.reactiveredis.datamodel.RoomInput;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyExtractors;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;
import static org.springframework.web.reactive.function.server.ServerResponse.status;


@Component
@Slf4j
public class APIHandler {

    @Autowired
    private RoomService service;

    public Mono<ServerResponse> findSpecificRoom(ServerRequest request){
        return request.bodyToMono(RoomInput.class)
                .log()
                .flatMap(s -> service.getRequestedRoom(s.getId()))
                .flatMap(s -> ok().body(BodyInserters.fromObject(s)));
    }

    public Mono<ServerResponse> getSuggestion(ServerRequest request){

        return request.bodyToMono(RoomInput.class)
                .filter(s -> s.getParticipants() > 0)
                .flatMap(s -> service.getSuggestions(s.getParticipants()))
                .flatMap(s -> ok().body(BodyInserters.fromObject(s)));
    }

    public Mono<ServerResponse> getRoomNameAndID(ServerRequest request){
        return service.getRoomNamesAndID()
                .flatMap(s -> ok().body(BodyInserters.fromObject(s)))
                .doOnError(s -> status(HttpStatus.INTERNAL_SERVER_ERROR).body(BodyInserters.fromObject(s.getMessage())));
    }

}
