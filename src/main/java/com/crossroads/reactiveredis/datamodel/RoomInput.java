package com.crossroads.reactiveredis.datamodel;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoomInput {
    private String id;
    private int participants;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
