package com.crossroads.reactiveredis.datamodel;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.index.Indexed;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Room {

    public @Id String roomID;
    public @Indexed String roomName;
    public int tables;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
