package com.crossroads.reactiveredis;

import com.crossroads.reactiveredis.datamodel.Room;
import com.crossroads.reactiveredis.datamodel.RoomNameOutput;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveListOperations;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class RoomService {

    @Autowired
    private ReactiveRedisOperations<String, Room> ops;

    @Autowired
    private ReactiveRedisConnectionFactory factory;

    public Flux<Room> getBestSuggestionRooms(int count){
        log.info("Finding suggestions for {} participants", count);
        return ops.keys("*")
                .log()
                .flatMap(ops.opsForValue()::get)
                .filter(s -> s.tables*4 >= count );

    }

    public Mono<List<Room>> getSuggestions(int count){
        return ops.keys("*")
                .log()
                .flatMap(ops.opsForValue()::get)
                .filter(s -> s.tables*4 >= count)
                .collectList();
    }

    public Mono<Room> getRequestedRoom(String uuid){
        return ops.keys(uuid)
                .log()
                .flatMap(ops.opsForValue()::get)
                .flatMap(s -> {
                    log.info(s.toString());
                    return Mono.just(s);})
                .next();

    }

    public Mono<List<RoomNameOutput>> getRoomNamesAndID(){
        return ops.keys("*")
                .flatMap(ops.opsForValue()::get)
                .map(s -> new RoomNameOutput(s.roomID, s.roomName))
                .collectList();
    }

    @PostConstruct
    private void loadData(){
        factory.getReactiveConnection().serverCommands().flushAll()
            .thenMany(
                Flux.just(
                    new Room(UUID.randomUUID().toString(), "Adonis", 4),
                    new Room(UUID.randomUUID().toString(), "Ajax", 4),
                    new Room(UUID.randomUUID().toString(), "Zeus", 6),
                    new Room(UUID.randomUUID().toString(), "Apollo", 6),
                    new Room(UUID.randomUUID().toString(), "Helios", 8),
                    new Room(UUID.randomUUID().toString(), "Zephyr", 10),
                    new Room(UUID.randomUUID().toString(), "Triton", 12),
                    new Room(UUID.randomUUID().toString(), "Bacchus", 2)
                )
                .flatMap(line -> ops.opsForValue().set(line.roomID, line))
                .thenMany(ops.keys("*"))
                .flatMap(ops.opsForValue()::get))
                .subscribe(ret -> log.info(ret.toString()));
    }

}
