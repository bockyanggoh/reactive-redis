package com.crossroads.reactiveredis.config;

import com.crossroads.reactiveredis.handler.APIHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class RouteConfig {

    @Bean
    public RouterFunction<ServerResponse> routes(APIHandler handler){
        return
                route(POST("/find"), handler::findSpecificRoom)
                .and(route(POST("/suggestion"), handler::getSuggestion))
                .and(route(GET("/fetchList"), handler::getRoomNameAndID));
    }


}
